---
name: Typography
---

The GitLab brand uses the Source Sans Pro font family. Headers (h1, h2, and so on) always have a weight of 600 (unless used in special situations like large, custom quotes) and the body text always has a weight of 400. Headers should not be given custom classes, they should be used as tags and tags alone (h1, h2, and so on) and their sizes or weights should not be changed, unless rare circumstances occur. Here are typography tags.

`H1: Header Level 1`

`H2: Header Level 2`

`H3: Header Level 3`

`H4: Header Level 4`

`p: Body text`
