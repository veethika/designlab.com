<!--
Start by choosing one of the following based on the nature of your contribution:
   - For changes to an existing file, duplicate the general template (https://www.figma.com/file/v7I3e2CqmRagoi5jSzjROQ/Title-group-project-1337) to the UX Foundations Figma project (https://www.figma.com/files/project/8421515/UX-Foundation). Update the file name and cover details (this new file is considered the “working file”). Make changes in the working file to detached instances of a component, or copy/paste from designs in the original (target) file.
   - For additions, duplicate the template that is most relevant (https://www.figma.com/files/project/5846042/Templates) to the project that also is the most relevant. For example, a file making a change to the component library would belong in the UX Foundations project. Update the file name and cover details (this new file is considered the “working file”) and proceed with the design.
   - For community contributions, create a new file in your drafts.

To move a duplicate file:
Open the duplicate, then use the dropdown next to the file name to select
“Move to Project…” and select the desired project as the new location. The duplicate is now the “working file.”
-->

### Description

<!-- Add a short description of your contribution. Consider adding
a checklist of variations, states, and breakpoints to the description so that reviewers can be sure to cross reference everything that has been completed. -->

### Figma file

<!-- Before pasting the link to your Figma file/frame, in the file sharing settings, make sure that “anyone with the link” can view. -->

[View working file in Figma →](ADD LINK TO FIGMA FILE/FRAME)

### Checklist

Make sure the following are completed before closing the issue:

1. [ ] **Assignee**: Design in your working file. When applicable, follow our 
   [structure][structure], [building][building], and [annotation][annotation] guidelines. If you have any questions, reach out to a [FE/UX Foundations designer][foundations-team].
1. [ ] **Assignee**: Update the link to the working file in the issue 
   description.
1. [ ] **Assignee**: Ask a [FE/UX Foundations designer][foundations-team]
   to review your design (ensure they have edit permissions in Figma).
1. [ ] **Reviewer**: Review and approve assignee’s design. Specific design 
   questions can be addressed with comments in Figma. Comment in this issue when the content is less specific to the design or requires greater visibility.
1. [ ] **Reviewer**: Assign to a [Figma maintainer][figma-maintainer]
   for final review (make sure they have edit permissions in Figma).
1. [ ] **Maintainer**: Review and approve assignee’s changes.
1. [ ] **Maintainer**: Add the changes or additions to the target file. For 
   example, you’d copy a final chart design from the working file and paste into the related location in the Data Visualization file.
     1. [ ] Ensure that all styles and components now belong to the target file.
     1. [ ] For changes to the **Component Library** file, view the components in the Assets panel to ensure they align with our [asset library structure guidelines][structure].
1. [ ] **Maintainer**: When applicable, [publish][publishing] any library 
   changes along with a clear commit message.
1. [ ] **Assignee**: Move your working file to the shared Figma project:
     1. [ ] For Component Library changes, move your file to the [**Component archive**][component-archive] project.
     1. [ ] For all other changes, move your file to the [**Misc archive**][misc-archive] project.
     1. [ ] If you’re a community contributor, we ask that you [transfer ownership of your draft file](https://help.figma.com/hc/en-us/articles/360040530853) to the maintainer so they can move it to our archive, along with its version history and comments.
1. [ ] **Assignee** (or Maintainer, for community contributions): If it’s a new
   pattern or a significant change, add an agenda item to the next UX weekly call to inform the team.
1. [ ] **Assignee**: When applicable, create a merge request in this repository 
   with the [component-guideline template][component-guideline-template]
   to create or update the component’s documentation page. 
1. [ ] **Assignee**: When applicable, [create an issue in GitLab UI][new-gitlab-ui-issue] to build or update the component code. Mark the new issue as related to this one.
1. [ ] **Assignee**: 🎉 Congrats, you made it! You can now close this issue.

/label ~"UX" ~"Figma"

[annotation]: https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/blob/master/doc/pajamas-ui-kit.md#adding-descriptions-notes-and-annotations
[building]: https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/blob/master/doc/pajamas-ui-kit.md#building-components
[foundations-team]: https://about.gitlab.com/company/team/?department=fe-ux-foundations-team
[figma-maintainer]: https://about.gitlab.com/handbook/engineering/projects/#design.gitlab.com
[publishing]: https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/blob/master/doc/pajamas-ui-kit.md#publishing-changes
[structure]: https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/blob/master/doc/pajamas-ui-kit.md#structure
[component-guideline-template]: https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/blob/master/.gitlab/merge_request_templates/component-guideline.md
[component-archive]: https://www.figma.com/files/project/5472112/Component-archive
[misc-archive]: https://www.figma.com/files/project/10620392/Misc-archive
[new-gitlab-ui-issue]: https://gitlab.com/gitlab-org/gitlab-ui/-/issues/new
